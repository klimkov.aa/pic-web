from flask_restful import Resource
from flask import jsonify


updated = {
    'status': True,
    'iter': 0
    }


'''
API for checking if the calculation logged the next portion of data
'''
class Updated(Resource):
    def get(self):
        return updated, 200, {'Access-Control-Allow-Origin': '*'}
    
    def post(self):
        updated['status'] = True

        if updated['status']:
            updated['iter'] += 1000

        return jsonify(updated)