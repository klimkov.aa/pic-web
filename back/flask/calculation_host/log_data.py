from flask_restful import Resource
from flask import request


'''API for getting the calculation log data'''
class LogData(Resource):
    Ntot_data = {}
    E_data = {}
    phi_data = {}
    rho_data = {}
    res = {}

    def get(self):
        type = request.args.get('type')
        

        if type == 'Ntot':
            LogData.res = {
                'status': 'ok',
                'Ntot': LogData.Ntot_data
                }
        elif type == 'E':
            LogData.res = {
                'status': 'ok',
                'E': LogData.E_data
                }
        elif type == 'rho':
            LogData.res = {
                'status': 'ok',
                'rho': LogData.rho_data
                }
        elif type == 'phi':
            LogData.res = {
                'status': 'ok',
                'phi': LogData.phi_data
                }

        try:
            return LogData.res, 200, {'Access-Control-Allow-Origin': '*'}
        except RuntimeError:
            return {'status': 'error'}
        
    def post(self):
        try:
            data = request.get_json()
            LogData.Ntot_data = data['Ntot']
            LogData.E_data = data['E']
            LogData.phi_data = data['phi']
            LogData.rho_data = data['rho']

            return {'updated': 'true'}
        except RuntimeError:
            return {'updated': 'error'}