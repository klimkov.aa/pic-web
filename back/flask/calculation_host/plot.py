from flask_restful import Resource
from flask import request
import requests

import numpy as np
import plotly.graph_objs as go
import plotly
import pandas as pd

import json


class Plot(Resource):
    def get(self):
        try:
            type = request.args.to_dict()['type']
            Ntot = requests.get('http://127.0.0.1:4000/api/log_data?type=Ntot').json()['Ntot']

            if len(Ntot) == 0:
                return {'status': 'getting_data'}, 200, {'Access-Control-Allow-Origin': '*'}
            else:
                if (type == 'Ntot'):
                    # ---Ntot---
                    df_Ntot = pd.DataFrame(np.array(Ntot), columns=['iter', 'electrons_Ntot', 'ions_Ntot'])
                    data_Ntot = [
                        go.Scattergl(
                            x=df_Ntot['iter'],
                            y=df_Ntot['electrons_Ntot'],
                            name='Electrons Ntot',
                            mode='lines'
                        ),
                        go.Scattergl(
                            x=df_Ntot['iter'], 
                            y=df_Ntot['ions_Ntot'],
                            name='Ions Ntot',
                            mode='lines'
                        )
                    ]
                    layout_Ntot = go.Layout(
                        title="Superparticles count",
                        xaxis=dict(
                            title="Iteration",
                            showgrid=True,
                            griddash = "dot",
                            gridwidth = 1,
                            gridcolor = "grey",
                            linewidth=1,
                            linecolor='black',
                            ticks='outside'
                        ),
                        yaxis=dict(
                            title="Ntot",
                            showgrid=True,
                            griddash = "dot",
                            gridwidth = 1,
                            gridcolor = "grey",
                            linewidth=1,
                            linecolor='black',
                            ticks='outside'
                        )) 
                    fig=go.Figure(layout=layout_Ntot,data=data_Ntot)
                    graph_Ntot = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

                    return {
                    'status': 'ok',
                      'Ntot': graph_Ntot,
                      'E': 'none',
                      'phi-distr': 'none',
                      'rho-distr': 'none',
                      'phi-profile': 'none',
                      'rho-profile': 'none'
                      }, 200, {'Access-Control-Allow-Origin': '*'}
                elif type == 'E':
                    #---E---
                    E = requests.get('http://127.0.0.1:4000/api/log_data?type=E').json()['E']
                    df_E = pd.DataFrame(np.array(E), columns=['iter', 'electrons_E', 'ions_E'])
                    data_E = [
                        go.Scatter(
                            x=df_E['iter'],
                            y=df_E['electrons_E'],
                            name='Electrons mean energy',
                            mode='lines'
                        ),
                        go.Scatter(
                            x=df_E['iter'], 
                            y=df_E['ions_E'],
                            name='Ions mean energy',
                            mode='lines'
                        )
                    ]
                    layout_E = go.Layout(
                        title="Mean energy",
                        xaxis=dict(
                            title="Iteration",
                            showgrid=True,
                            griddash = "dot",
                            gridwidth = 1,
                            gridcolor = "grey",
                            linewidth=1,
                            linecolor='black',
                            ticks='outside'
                        ),
                        yaxis=dict(
                            title="E, eV",
                            showgrid=True,
                            griddash = "dot",
                            gridwidth = 1,
                            gridcolor = "grey",
                            linewidth=1,
                            linecolor='black',
                            ticks='outside'
                        )) 
                    fig=go.Figure(layout=layout_E,data=data_E)
                    graph_E = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

                    return {
                    'status': 'ok',
                      'Ntot': 'none',
                      'E': graph_E,
                      'phi-distr': 'none',
                      'rho-distr': 'none',
                      'phi-profile': 'none',
                      'rho-profile': 'none'
                      }, 200, {'Access-Control-Allow-Origin': '*'}
                elif type == 'phi-distr':
                    #---phi_density---
                    phi = np.asarray(json.loads(
                            requests.get('http://127.0.0.1:4000/api/log_data?type=phi').json()['phi']
                            )['array'])
                    data_phi_distr = [
                        go.Heatmapgl(
                            z=phi,
                            x=np.arange(0, phi.shape[0]),
                            y=np.arange(0, phi.shape[1]),
                            colorscale='Plasma',
                            showscale=False
                        )
                    ]
                    layout_phi_distr = go.Layout(
                        title="Potential distribution",
                        xaxis=dict(
                            title="Cell x",
                            tickmode="array"
                        ),
                        yaxis=dict(
                            title="Cell y",
                            tickmode="array"
                        ),
                        autosize=True) 
                    fig=go.Figure(layout=layout_phi_distr,data=data_phi_distr)
                    graph_phi_distr = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

                    return {
                    'status': 'ok',
                      'Ntot': 'none',
                      'E': 'none',
                      'phi-distr': graph_phi_distr,
                      'rho-distr': 'none',
                      'phi-profile': 'none',
                      'rho-profile': 'none'
                      }, 200, {'Access-Control-Allow-Origin': '*'}
                elif type == 'rho-distr':
                    #---rho_density---
                    rho = np.asarray(json.loads(
                            requests.get('http://127.0.0.1:4000/api/log_data?type=rho').json()['rho']
                            )['array'])
                    data_rho_distr = [
                        go.Heatmapgl(
                            z=rho
                        )
                    ]
                    layout_rho_distr = go.Layout(
                        title="Charge density distribution",
                        xaxis=dict(
                            title="Cell x",
                            tickmode="array"
                        ),
                        yaxis=dict(
                            title="Cell y",
                            tickmode="array"
                        ),
                        autosize=True) 
                    fig=go.Figure(layout=layout_rho_distr,data=data_rho_distr)
                    graph_rho_distr = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

                    return {
                    'status': 'ok',
                      'Ntot': 'none',
                      'E': 'none',
                      'phi-distr': 'none',
                      'rho-distr': graph_rho_distr,
                      'phi-profile': 'none',
                      'rho-profile': 'none'
                      }, 200, {'Access-Control-Allow-Origin': '*'}
                elif type == 'phi-profile':
                    #---phi_profile---
                    phi = np.asarray(json.loads(
                            requests.get('http://127.0.0.1:4000/api/log_data?type=phi').json()['phi']
                            )['array'])
                    Nx = phi.shape[0]
                    df_phi_profile = pd.DataFrame(np.average(phi, axis=0), columns=['phi'])
                    data_phi_profile = [
                        go.Scatter(
                            x=np.arange(-Nx//2, Nx//2),
                            y=df_phi_profile['phi'],
                            mode='lines'
                        )
                    ]
                    layout_phi_profile = go.Layout(
                        title="Potential profile",
                        xaxis=dict(
                            title="Cell num",
                            showgrid=True,
                            griddash = "dot",
                            gridwidth = 1,
                            gridcolor = "grey",
                            linewidth=1,
                            linecolor='black',
                            ticks='outside'
                        ),
                        yaxis=dict(
                            title="Phi, V",
                            showgrid=True,
                            griddash = "dot",
                            gridwidth = 1,
                            gridcolor = "grey",
                            linewidth=1,
                            linecolor='black',
                            ticks='outside'
                        )) 
                    fig=go.Figure(layout=layout_phi_profile,data=data_phi_profile)
                    graph_phi_profile = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

                    return {
                    'status': 'ok',
                      'Ntot': 'none',
                      'E': 'none',
                      'phi-distr': 'none',
                      'rho-distr': 'none',
                      'phi-profile': graph_phi_profile,
                      'rho-profile': 'none'
                      }, 200, {'Access-Control-Allow-Origin': '*'}
                elif type == 'rho-profile':
                    #---rho_profile---
                    rho = np.asarray(json.loads(
                            requests.get('http://127.0.0.1:4000/api/log_data?type=rho').json()['rho']
                            )['array'])
                    df_rho_profile = pd.DataFrame(np.average(rho, axis=0), columns=['rho'])
                    data_rho_profile = [
                        go.Scatter(
                            x=np.arange(-Nx//2, Nx//2),
                            y=df_rho_profile['rho'],
                            mode='lines'
                        )
                    ]
                    layout_rho_profile = go.Layout(
                        title="Charge density profile",
                        xaxis=dict(
                            title="Cell num",
                            showgrid=True,
                            griddash = "dot",
                            gridwidth = 1,
                            gridcolor = "grey",
                            linewidth=1,
                            linecolor='black',
                            ticks='outside'
                        ),
                        yaxis=dict(
                            title=r'Rho, $m^-3$',
                            showgrid=True,
                            griddash = "dot",
                            gridwidth = 1,
                            gridcolor = "grey",
                            linewidth=1,
                            linecolor='black',
                            ticks='outside'
                        )) 
                    fig=go.Figure(layout=layout_rho_profile,data=data_rho_profile)
                    graph_rho_profile = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

                    return {
                    'status': 'ok',
                      'Ntot': 'none',
                      'E': 'none',
                      'phi-distr': 'none',
                      'rho-distr': 'none',
                      'phi-profile': 'none',
                      'rho-profile': graph_rho_profile
                      }, 200, {'Access-Control-Allow-Origin': '*'}

        except RuntimeError:
            return {'status': 'error'}

