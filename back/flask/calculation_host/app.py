from flask import Flask
from flask_restful import Api, Resource
from flask_executor import Executor

from deploy_config import DeployConfig
from updated import Updated
from log_data import LogData
from plot import Plot

import os
import importlib
import sys


app = Flask(__name__)
api = Api(app)

app.config['EXECUTOR_TYPE'] = 'thread'
app.config['EXECUTOR_PROPAGATE_EXCEPTIONS'] = True


pic_install_dir = os.getenv('PIC_INSTALL_DIR')


executor = Executor(app)

@executor.job
def create_executor():
    problem_spec = importlib.util.spec_from_file_location("problem", f'{pic_install_dir}/pic/test/python/test-simulation.py')
    problem = importlib.util.module_from_spec(problem_spec)
    sys.modules["problem"] = problem
    problem_spec.loader.exec_module(problem)
    problem.start(is_web=True) 


'''API for calculation handling'''
state = 'not_started'
class Calculation(Resource):
    def get(self):
        return {'state': executor.futures._state('task')}
    
    def post(self):
        try:
            create_executor.submit_stored('task')
            state = 'in_progress'

            return {
                'execute': 'ok',
                'calculation': {
                    'state': state
                }
            }
        except RuntimeError:
            state = 'error_while_execute'
            return {
                'execute': 'failed',
                'calculation': {
                    'state': state
                }
            }
        

'''API for stopping the calculation'''
class CalculationStop(Resource):
    def get(self):
        return {'state': executor.futures._state('task')}, 200, {'Access-Control-Allow-Origin': '*'}
    
    def post(self):
        try:
            executor.futures.pop('task')

            return {'stopping': 'success'}
        except RuntimeError:
            
            return {'stoppring': 'error'}

api.add_resource(DeployConfig, '/api/deploy_config')
api.add_resource(Updated, '/api/status')
api.add_resource(Calculation, '/api/execute')
api.add_resource(CalculationStop, '/api/execute/stop')
api.add_resource(LogData, '/api/log_data')
api.add_resource(Plot, '/api/plot')

executor.init_app(app)


def create_app():
    app.run(host='0.0.0.0', port=4000, debug=True)

if __name__ == '__main__':
    create_app()
