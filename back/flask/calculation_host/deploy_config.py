from flask_restful import Resource
from flask import request, redirect
import json
import os
import requests



conf = {}
pic_dir = os.getenv('PIC_DIR')
pic_install_dir = os.getenv('PIC_INSTALL_DIR')
front_host = 'localhost'


'''
API aimed to launch and hold on the calculation
'''
class DeployConfig(Resource):
    def get(self, param=None):
        return {param: conf[param]}

    def post(self):
        conf = request.form.to_dict()

        with open(f'{pic_install_dir}/pic/settings/config.json', "w") as outfile:
            json.dump(create_conf(conf), outfile)
    
        requests.post('http://127.0.0.1:4000/api/execute')
        
        return redirect(f'http://{front_host}:3000/', code=302)
    

def create_conf(conf):
    return {
            "geom_params": {
                "system_radius": conf["system_radius"],
                "scaling_factor": conf["scaling_factor"]
            },
            "init_plasmas_params": {
                "electrons_temperature_ev": conf["electrons_temperature_ev"],
                "ions_temperature_ev": conf["ions_temperature_ev"],
                "neutrals_pressure": conf["neutrals_pressure"],
                "electrons_density": conf["electrons_density"],
                "ions_density": conf["ions_density"],
                "magnetic_field": conf["magnetic_field"],
                "m_ion_in_electron_mass": conf["m_ion_in_electron_mass"],
                "gas_temperature": conf["gas_temperature"],
                "I_hot": conf["I_hot"],
                "I_cold": conf["I_cold"],
                "initial_injection_energy_ev": conf["initial_injection_energy_ev"]
            },
            "gyro_coeff": conf["gyro_coeff"],
            "ppc": conf["ppc"],
            "thread_pool_size": conf["thread_pool_size"],
            "ion_cathode_leave_params": {
                "gamma": conf["gamma"],
                "radius_leave_min_m": conf["radius_leave_min_m"],
                "radius_leave_max_m": conf["radius_leave_max_m"],
                "ion_leave_step": conf["ion_leave_step"]
            },
            "electron_emission_params": {
                "energy_emission_cold_ev": conf["energy_emission_cold_ev"],
                "gamma": conf["gamma"],
                "electron_emission_cold_step": conf["electron_emission_cold_step"],
                "electron_emission_radius_cold_min_m": conf["electron_emission_radius_cold_min_m"],
                "electron_emission_radius_cold_max_m": conf["electron_emission_radius_cold_max_m"],
                "energy_emission_hot_ev": conf["energy_emission_hot_ev"],
                "electron_emission_hot_step": conf["electron_emission_hot_step"],
                "electron_emission_radius_hot_min_m": conf["electron_emission_radius_hot_min_m"],
                "electron_emission_radius_hot_max_m": conf["electron_emission_radius_hot_max_m"]
            },
            "collision_params": {
                "collision_step_electron": conf["collision_step_electron"],
                "elastic_electron_sigma_path": f'{pic_dir}/pic/test/cross_section_data/e-Ar_elastic.txt',
                "ionization_sigma_path":       f'{pic_dir}/pic/test/cross_section_data/e-Ar_ionization.txt',
                "elastic_neutrals_sigma_path":   f'{pic_dir}/pic/test/cross_section_data/Ar+-Ar_elastic.txt'
            },
            "pic_cycle_params": {
                "iterations_number": conf["iterations_number"]
            },
            "log_info" : {
                "basic_log_file_path" : f'{pic_dir}/pic/test/Log/log.txt',
                "Ntot_log_step" : 1000,
                "Ntot_log_file_path" : f'{pic_dir}/pic/test/Log/Ntot_log.txt',
                "energy_log_step" : 1000,
                "energy_log_file_path" : f'{pic_dir}/pic/test/Log/energy_log.txt',
                "phi_log_step": 1000,
                "phi_log_file_prefix" : f'{pic_dir}/pic/test/Log/phi/phi',
                "Ex_log_step": 1000,
                "Ex_log_file_prefix" : f'{pic_dir}/pic/test/Log/Ex/Ex',
                "Ey_log_step": 1000,
                "Ey_log_file_prefix" : f'{pic_dir}/pic/test/Log/Ey/Ey',
                "rho_e_log_step": 1000,
                "rho_e_log_file_prefix" : f'{pic_dir}/pic/test/Log/rho_e/rho_e',
                "rho_i_log_step": 1000,
                "rho_i_log_file_prefix" : f'{pic_dir}/pic/test/Log/rho_i/rho_i',
                "rho_log_step": 1000,
                "rho_log_file_prefix" : f'{pic_dir}/pic/test/Log/rho/rho'
            },
            "plot_info" : {
                "Ntot_fig_file_name" : f'{pic_dir}/pic/test/Log/Plots/Ntot.png',
                "mean_energy_fig_file_name" : f'{pic_dir}/pic/test/Log/Plots/mean_energy.png',
                "phi_fig_file_name" : f'{pic_dir}/pic/test/Log/Plots/phi.png',
                "Ex_fig_file_name" : f'{pic_dir}/pic/test/Log/Plots/Ex.png',
                "Ey_fig_file_name" : f'{pic_dir}/pic/test/Log/Plots/Ey.png',
                "rho_e_fig_file_name" : f'{pic_dir}/pic/test/Log/Plots/rho_e.png',
                "rho_i_fig_file_name" : f'{pic_dir}/pic/test/Log/Plots/rho_i.png',
                "rho_fig_file_name" : f'{pic_dir}/pic/test/Log/Plots/rho.png',
                "anim_file_prefix" : f'{pic_dir}/pic/test/Log/Plots'
            }
        }
