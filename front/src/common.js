export const values = {
    "Radius of the system, m": 'system_radius',
    "Scaling factor (0, 1]": 'scaling_factor',
    "Electrons initial temperature, eV": 'electrons_temperature_ev',
    "Ions initial temperature, eV": 'ions_temperature_ev',
    "Neutrals initial pressure, Pa": 'neutrals_pressure',
    "Electrons initial density, m^-3": 'electrons_density',
    "Ions initial density, m^-3": 'ions_density',
    "Axial component of magnetic field (Bz), T": 'magnetic_field',
    "Ions mass, m_e": 'm_ion_in_electron_mass',
    "Neutrals temperature, K": 'gas_temperature',
    "Hot cathode current, A": 'I_hot',
    "Cold cathode current, A": 'I_cold',
    "Initial emission energy, eV": 'initial_injection_energy_ev',
    "Gyrokinetic coefficient": 'gyro_coeff',
    "Particles-per-cell (ppc)": 'ppc',
    "Thread pool size": 'thread_pool_size',
    "Secondary emission coefficient (0, 1]": 'gamma',
    "Min ions leave radius, m": 'radius_leave_min_m',
    "Max ion leave radius, m": 'radius_leave_max_m',
    "Ions leave step": 'ion_leave_step',
    "Cold cathode emission energy, eV": 'energy_emission_cold_ev',
    "Cold cathode emission step": 'electron_emission_cold_step',
    "Min cold cathode radius, m": 'electron_emission_radius_cold_min_m',
    "Max cold cathode radius, m": 'electron_emission_radius_cold_max_m',
    "Hot cathode emission energy, eV": 'energy_emission_hot_ev',
    "Hot cathode emission step": 'electron_emission_hot_step',
    "Min hot cathode radius, m": 'electron_emission_radius_hot_min_m',
    "Max hot cathode radius, m": 'electron_emission_radius_hot_max_m',
    "Electron-neutral collision step": 'collision_step_electron',
    "Iterations number": 'iterations_number'
}

export const defaultValues = {
    "system_radius": '0.26',
    "scaling_factor": '0.02',
    "electrons_temperature_ev": '10.0',
    "ions_temperature_ev": '10.0',
    "neutrals_pressure": '0.532',
    "electrons_density": '1e15',
    "ions_density": '1e15',
    "magnetic_field": '0.1',
    "m_ion_in_electron_mass": '500.0',
    "gas_temperature": '500.0',
    "I_hot": '6.0',
    "I_cold": '4.0',
    "initial_injection_energy_ev": '0.1',
    "gyro_coeff": '100',
    "ppc": '1',
    "thread_pool_size": '8',
    "gamma": '0.1',
    "radius_leave_min_m": '0.01',
    "radius_leave_max_m": '0.15',
    "ion_leave_step": '500',
    "energy_emission_cold_ev": '100.0',
    "electron_emission_cold_step": '500',
    "electron_emission_radius_cold_min_m": '0.01',
    "electron_emission_radius_cold_max_m": '0.15',
    "energy_emission_hot_ev": '100.0',
    "electron_emission_hot_step": '500',
    "electron_emission_radius_hot_min_m": '0.0',
    "electron_emission_radius_hot_max_m": '0.01',
    "collision_step_electron": '5',
    "iterations_number": '1e6'
}

export const validateForm = (form) => {
    for (let field in form) {
        if (field.value === "") {
            alert(field.name + 'must not be empty!');
            return false;
        }
    }

    return true;
}