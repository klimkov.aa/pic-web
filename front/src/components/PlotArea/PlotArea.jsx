import React, {useState, useEffect} from "react";

import {PlotlyPlot} from "../Plot/Plot";
import './PlotArea.css'

export const PlotArea = () => {
    const values = {
        Ntot: 'Ntot',
        E: 'E',
        rhoProfile: 'rho-profile',
        phiProfile: 'phi-profile',
        rhoDistr: 'rho-distr',
        phiDistr: 'phi-distr'
    }
    const [plot, setPlot] = useState(values.Ntot);
    const [iter, setIter] = useState(0);
    useEffect(() => {
        const interval = setInterval(() => {
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Accept', 'application/json');
            headers.append('Access-Control-Allow-Origin', 'http://localhost:3000')
            headers.append('Access-Control-Allow-Credentials', 'true');

            fetch(`http://localhost:4000/api/status`)
            .then(response => response.json())
            .then((json) => setIter(json['iter']))
            .catch((err) => console.log(err));
            }, 5000);
        return () => clearInterval(interval);
      }, []);

    return (
        <div className={"plot-area"}>
            <div className={'plot-select'}>
                <div>Plot type --</div>
                <div className={'combobox'}>
                    <select onChange={event => {
                        setPlot(event.target.value)
                    }}>
                        <option value={values.Ntot}>Superparticles number</option>
                        <option value={values.E}>Mean energy</option>
                        <option value={values.rhoProfile}>Charge density profile</option>
                        <option value={values.phiProfile}>Potential profile</option>
                        <option value={values.rhoDistr}>Charge density distribution</option>
                        <option value={values.phiDistr}>Potential distribution</option>
                    </select>
                </div>
            </div>
            <div>
                <label className="iter-count">Calculated {iter} iterations</label>
                <PlotlyPlot type={plot} iteration={iter}/>
            </div>
        </div>
    )
}