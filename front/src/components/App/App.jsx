import React, { useState } from "react";
import "./App.css";

import {FormRow} from "../FormRow/FormRow";
import {PlotArea} from "../PlotArea/PlotArea";

import {values, defaultValues} from "../../common.js"

export const App = () => {
    const [isInvalid, setIsInvalid] = useState(false);
    return (
        <div className={'wrapper'}>
            <div className={'header'}>
                <div className={'pic__header'}>
                    <em>PIC Web Application</em>
                </div>
                <div className={'pic__description'}>
                    The app is developed for LaPlaS plasma mass-separation process modeling
                    using PIC method.
                </div>
            </div>
            <div className={'grid__main-screen'}>
                <div className={'screen-part'}>
                    <form id="main-form" name="main-form" action={`http://localhost:4000/api/deploy_config`} method={"post"} onSubmit={(e) => {
                        if (isInvalid){
                            e.preventDefault();
                            alert('All the values must have values!');
                            setIsInvalid(false);
                        }
                    }}>
                        {
                            Object.entries(values).map(([eng_name, json_name]) => {
                                return (<FormRow name={json_name} eng_name={eng_name} key={json_name} defaultValue={defaultValues[json_name]} id={json_name} setState={setIsInvalid}/>)
                            })
                        }
                        <div className={'button-submit'}>
                            <input type="submit" value={"Execute"} title="Execute calculation with set params"/>
                        </div>
                    </form>
                </div>
                <div className={'screen-part'}>
                    <PlotArea id={Date.now()}/>
                </div>
            </div>
        </div>
    )
}
