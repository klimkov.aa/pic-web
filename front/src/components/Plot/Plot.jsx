import React, {useState, useEffect} from "react";
import ReactDOM from 'react-dom/client';

import Plot from "react-plotly.js";
import './Plot.css'


export const PlotlyPlot = ({type, iteration}) => {
    const [data, setData] = useState({});
    useEffect(() => {
        console.log(type);
        const interval = setInterval(() => {
            fetch(`http://localhost:4000/api/plot?type=` + type)
            .then(response => response.json())
            .then(json => {
                data.layout = null;
                data.data = null;
                setData(JSON.parse(json[type]));
            })
            .catch(err => console.log(err));
        }, 3000);

        return () => {
            clearInterval(interval);
        }
    }, [type]);

    if (iteration > 0){
        return (
            <div className="plot-holder">
                <Plot data={data.data} layout={data.layout}/>
            </div>
        )
    } else {
        return (
            <div className="plot-holder">
                <img src={require('../../plots/load.gif')} alt="loading..." />
            </div>
        );
    }
}