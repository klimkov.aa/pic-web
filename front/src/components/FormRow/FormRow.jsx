import React from "react";
import "./FormRow.css"

export const FormRow = ({name, eng_name, defaultValue, id, setState}) => {
    return (
            <div className={"grid__form-row"}>
                <div className={'cell-name'}>{eng_name}</div>
                <div className={'input-div'}>
                    <input name={name} key={name} className="input" type="text" defaultValue={defaultValue} id={id} onChange={e => {
                        if (e.target.value == ""){
                            setState(true);
                        }
                    }}/>
                </div>
            </div>
    )
}