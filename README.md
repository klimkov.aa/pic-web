# How to get started?

## Get core PIC code from repo https://gitlab.stellartermite.keenetic.link/sergey/pic/-/tree/dev

Place the local repo on your local machine usign the path /path/to/pic/repo

## Make coorections to the docker-compose.yaml file according to the route to PIC repo you have chosen before

For instance if the route if /Users/username/Documents and the pic folder is placed on this level. So that the pic folder is 
/Users/username/Documents/pic.
Then you should replace values corresponding the keys [context, dockerfile, volumes] in the way like the following:

build: 
    context: /Users/username/
    dockerfile: ./Documents/pic-web/back/flask/calculation_host/Dockerfile
volumes:
    - /Users/username/Documents/pic-web/back:/home/pic-web/back:ro



## Make corrections to pic-web/back/flask/calculation_host/Dockerfile

All you need to implement during current step proceeding is to replace Documents folder to the folder you have chosen to store the core PIC code

For instance if you store code in folder corresponding the route /Users/username/Documents/pic_folder/pic then replace Documents/ folder with the following: Documents/pic_folder/...

## Launch containers

During this step proceeding you just need to install Docker Engine to your local machine according the instructions provided by the official web-cite of Docker: https://docs.docker.com/engine/install/

After you`re done with the previous step, you just need to change your folder to the route folder of pic-web and run the following command in the terminal: docker compose build && docker compose up